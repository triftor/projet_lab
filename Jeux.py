import random

tab = []
i = 0
while(i < 20):
    tab.append(random.randint(-20, 20))
    i = i + 1
print(tab)

player1 = [50, 0]
player2 = [50, 0]

print("le joueur 1 est sur la case " , player1[1] , "et a " , player1[0], "euros")

print("le joueur 2 est sur la case " , player2[1] , "et a " , player2[0], "euros")

while player1[0] > 0 and player2[0] > 0:
    dices = random.randint(2, 12)
    if (player1[1] + dices) >= 20 :
        player1[1] = player1[1] + dices - 20
    else: player1[1] = player1[1] + dices
    player1[0] = player1[0] + tab[player1[1]]
    print("le joueur 1 a fait ", dices, "il tombe sur la case ", player1[1], "il a ", player1[0], "euros")

    dices = random.randint(2, 12)
    if (player2[1] + dices) >= 20:
        player2[1] = player2[1] + dices - 20
    else:
        player2[1] = player2[1] + dices
    player2[0] = player2[0] + tab[player2[1]]
    print("le joueur 2 a fait ", dices, "il tombe sur la case ", player2[1], "il a ", player2[0], "euros")

if player1[0] <= 0:
    print("le joueur 2 a gagné")

else:
    print("le joueur 1 a gagné ")